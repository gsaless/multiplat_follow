
int xPlayer = 0;
int yPlayer = 20;
int sizePlayer = 20;

float xNpc = 0;
float yNpc = 0;
int sizeNpc = 20;

float incX = 0;
float incY = 0;
float speed = 0.1;

void setup(){
  size(1000, 1000);
}

void draw(){
  background(0);
  
  player();
  npc();
}

void player(){
  xPlayer = mouseX;
  yPlayer = mouseY;
  
  rect(xPlayer, yPlayer, sizePlayer, sizePlayer);  
}

void npc(){
  if(xNpc < xPlayer){ 
    incX += speed; 
  }
  
  if(yNpc < yPlayer){ 
    incY += speed; 
  }
  
  if(xNpc > xPlayer){ 
    incX -= speed; 
  }
  
  if(yNpc > yPlayer){ 
    incY -= speed; 
  }
  
  xNpc += incX;
  yNpc += incY;
  
  ellipse(xNpc, yNpc, sizeNpc, sizeNpc);  
}

